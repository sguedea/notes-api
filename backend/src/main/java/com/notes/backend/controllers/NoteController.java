package com.notes.backend.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.notes.backend.models.Note;
import com.notes.backend.repositories.NoteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
public class NoteController {
    

    @Autowired
    NoteRepository noteRepo;

    @GetMapping("/notes")
    public ResponseEntity<List<Note>> getAllNotes(@RequestParam(required = false) String title) {
        try {
            List<Note> note = new ArrayList<Note>();

            if (title == null) {
                noteRepo.findAll().forEach(note::add);
            }
            else {
                noteRepo.findByTitleContaining(title).forEach(note::add);
            }

            if (note.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(note, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/notes/{id}")
    public ResponseEntity<Note> getNoteById(@PathVariable("id") long id) {
        Optional<Note> noteData = noteRepo.findById(id);

        if (noteData.isPresent()) {
            return new ResponseEntity<>(noteData.get(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_EXTENDED);
        }
    }

    @PostMapping("/notes")
    public ResponseEntity<Note> createNote(@RequestBody Note note) {
        try {
            Note n = noteRepo.save(new Note(note.getTitle(), note.getDescription(), false));
            return new ResponseEntity<>(n, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/notes/{id}")
    public ResponseEntity<Note> updateNote(@PathVariable("id") long id, @RequestBody Note note) {
        Optional<Note> noteData = noteRepo.findById(id);

        if (noteData.isPresent()) {
            Note n = noteData.get();
            n.setTitle(note.getTitle());
            n.setDescription(note.getDescription());
            n.setPublished(note.isPublished());

            return new ResponseEntity<>(noteRepo.save(n), HttpStatus.OK);

        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/notes/{id}")
    public ResponseEntity<HttpStatus> deleteNote(@PathVariable("id") long id) {
        try {
            noteRepo.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/note")
    public ResponseEntity<HttpStatus> deleteAll() {
        try {
            noteRepo.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/notes/published")
    public ResponseEntity<List<Note>> findByPublished() {
        try {
            List<Note> n = noteRepo.findByPublished(true);

            if (n.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(n, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /*
NOTES:
– @CrossOrigin is for configuring allowed origins.
– @RestController annotation is used to define a controller and to indicate that the return value of the methods should be be bound to the web response body.
– @RequestMapping("/api") declares that all Apis’ url in the controller will start with /api.
– We use @Autowired to inject TutorialRepository bean to local variable.
    */

}
