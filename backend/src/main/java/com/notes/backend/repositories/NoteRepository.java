package com.notes.backend.repositories;

import java.util.List;

import com.notes.backend.models.Note;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteRepository extends JpaRepository<Note, Long> {

    List<Note> findByPublished(boolean published);

    List<Note> findByTitleContaining(String title);

    

    /*
NOTES:

Now we can use JpaRepository’s methods: save(), findOne(), findById(), findAll(), count(), delete(), deleteById()… without implementing these methods.

We also define custom finder methods:
– findByPublished(): returns all Tutorials with published having value as input published.
– findByTitleContaining(): returns all Tutorials which title contains input title.
    */
}
